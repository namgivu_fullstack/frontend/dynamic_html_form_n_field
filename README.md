We want to have subform, aka a group of formfield in a big form.
In a given form, we want to have radio button that display different subforms when selected
Also, in a subform, we want to have dynamic fieldgroup that can be added/removed

At the end, those selected subform, and list of subfield will be submitted into data/reqbody from an `$.ajax() request`
